# ci-images

Set of build images 👊.

| Image                                      | Description                                     |
| ------------------------------------------ | ----------------------------------------------- |
| `registry.gitlab.com/ct0r/ci-images/git`   | Alpine linux with git.                          |
| `registry.gitlab.com/ct0r/ci-images/ssh`   | Alpine linux with ssh client.                   |
| `registry.gitlab.com/ct0r/ci-images/s3cmd` | Alpine linux with [s3cmd].                      |
| `registry.gitlab.com/ct0r/ci-images/node`  | Alpine linux with node:lts, git and ssh client. |

[s3cmd]: https://github.com/s3tools/s3cmd
